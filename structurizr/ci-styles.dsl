        # color -> Color de las relaciones
        # colour -> Color de texto
        # stroke -> Color de contorno
        branding {
            font Ambit
        }
        styles {
            element Element {
                colour #de425b
                background #e6eeec
                stroke #de425b
                width 450
                height 300
                fontSize 24
                border solid
                opacity 100
                metadata true
                description false
            }
            element "Person" {
                shape Person
                background #de425b
                colour #d1e8e2
                width 250
                height 250
            }
            element "Robot" {
                shape Robot
                background #de425b
                colour #d1e8e2
                width 250
                height 250
            }
            element "Library" {
                shape Folder
                background #f3babc
                colour #de425b
            }
            element "Container" {
                shape RoundedBox
                background #A9D6E5
                colour #00246d
            }
            element "Software System" {
                shape RoundedBox
                background #00246d
                colour #bfbfbf
            }
            element "WebBrowser" {
                shape WebBrowser
            }
            element "Movil" {
                shape MobileDevicePortrait
                width 200
                height 300
            }
            element "Mobile" {
                shape MobileDevicePortrait
            }
            element "Tablet" {
                shape MobileDeviceLandscape
            }
            element "Repository" {
                shape Cylinder
                background #D1E8E2
                colour #19747E
            }
            element "MessageBroker" {
                shape Pipe
                background #D1E8E2
                colour #19747E
            }
            element "MessageBus" {
                shape Pipe
                background #D1E8E2
                colour #19747E
            }
            element "EventBus" {
                shape Pipe
                background #D1E8E2
                colour #19747E
            }

            # Estilo por default para relaciones
            relationship "Relationship" {
                color #404040
                dashed false
                width 400
            }
            # Estilo para relaciones asincronas
            relationship "Async" {
                color #404040
                dashed true
            }
            relationship "Pub" {
                color #404040
                dashed true
            }
            relationship "Message" {
                color #404040
                dashed true
            }
            relationship "Pub" {
                color #404040
                dashed true
            }
            relationship "Push" {
                color #404040
                dashed true
            }
        }