        # color -> Color de las relaciones
        # colour -> Color de texto
        # stroke -> Color de contorno
        branding {
            logo https://www.elpuertodeliverpool.mx/img/logo-el-puerto.png
            font Ambit
        }
        styles {
            element Element {
                colour #e51d9c
                stroke #8A8D8F
                width 450
                height 300
                fontSize 24
                border solid
                opacity 100
                metadata true
                description false
            }
            element "Person" {
                shape Person
                background #e51d9c
                colour #bfbfbf
                width 250
                height 250
            }
            element "Robot" {
                shape Robot
                background #e51d9c
                colour #bfbfbf
                width 250
                height 250
            }
            element "Library" {
                shape Folder
                background #d4c99f
                colour #ff808b
            }
            element "Container" {
                shape Hexagon
                background #84a7e3
                colour #00246d
            }
            element "Software System" {
                shape RoundedBox
                background #00246d
                colour #f4a6d7
            }
            element "WebBrowser" {
                shape WebBrowser
            }
            element "Movil" {
                shape MobileDevicePortrait
                width 200
                height 300
            }
            element "Mobile" {
                shape MobileDevicePortrait
            }
            element "Tablet" {
                shape MobileDeviceLandscape
            }
            element "Repository" {
                shape Cylinder
                background #f4a6d7
                colour #00246d
            }
            element "MessageBroker" {
                shape Pipe
                background #00246d
                colour #84a7e3
            }
            element "MessageBus" {
                shape Pipe
                background #00246d
                colour #84a7e3
            }
            element "EventBus" {
                shape Pipe
                background #00246d
                colour #84a7e3
            }

            # Estilo por default para relaciones
            relationship "Relationship" {
                color #e056bd
                dashed false
                width 400
            }
            # Estilo para relaciones asincronas
            relationship "Async" {
                color #84a7e3
                dashed true
            }
            relationship "Pub" {
                color #84a7e3
                dashed true
            }
            relationship "Message" {
                color #84a7e3
                dashed true
            }
            relationship "Pub" {
                color #84a7e3
                dashed true
            }
            relationship "Push" {
                color #84a7e3
                dashed true
            }
        }